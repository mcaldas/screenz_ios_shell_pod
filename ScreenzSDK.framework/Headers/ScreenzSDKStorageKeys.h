//
//  ScreenzSDKStorageKeys.h
//  ScreenzSDK
//
//  Created by Sebastian Castro on 2/5/16.
//  Copyright © 2016 Screenz. All rights reserved.
//

#ifndef ScreenzSDKStorageKeys_h
#define ScreenzSDKStorageKeys_h

#define kSSDK_LOCAL_CONFIG_PID          @"pid"
#define kSSDK_LOCAL_CONFIG_isQA         @"isQA"
#define kSSDK_LOCAL_CONFIG_PushPageId   @"pushPageId"
#define kSSDK_LOCAL_CONFIG_MsgId        @"msgId"
#define kSSDK_LOCAL_CONFIG_UDID         @"udid"
#define kSSDK_LOCAL_CONFIG_GPlusId      @"gplusid"
#define kSSDK_LOCAL_CONFIG_TWid         @"twid"
#define kSSDK_LOCAL_CONFIG_INSid        @"instagramid"
#define kSSDK_LOCAL_CONFIG_FBid         @"fbid"
#define kSSDK_LOCAL_CONFIG_Did          @"disneyid"

#endif /* ScreenzSDKStorageKeys_h */
