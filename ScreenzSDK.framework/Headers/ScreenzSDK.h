//
//  ScreenzSDK.h
//  ScreenzSDK
//
//  Created by Sebastian Castro on 2/4/16.
//  Copyright © 2016 Screenz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//! Project version number for ScreenzSDK.
FOUNDATION_EXPORT double ScreenzSDKVersionNumber;

//! Project version string for ScreenzSDK.
FOUNDATION_EXPORT const unsigned char ScreenzSDKVersionString[];

#import <ScreenzSDK/ScreenzSDKManager.h>
#import <ScreenzSDK/ScreenzSDKAppConfigModel.h>
#import <ScreenzSDK/ScreenzSDKSchemeValues.h>
#import <ScreenzSDK/ScreenzSDKServerDataModel.h>
#import <ScreenzSDK/ScreenzSDKStorageKeys.h>
#import <ScreenzSDK/ScreenzSDKBaseViewController.h>
#import <ScreenzSDK/ScreenzSDKLoadingViewController.h>
#import <ScreenzSDK/ScreenzSDKMediaUploadModel.h>

/**
 * - Libraries versions -
 *
 * Location Manager 4.1.1 https://github.com/intuit/LocationManager
 *
 * JSONModel 1.2.0 https://github.com/icanzilb/JSONModel
 *
 * AFNetworking 3.0.4 http://afnetworking.com/
 *
 * WebViewJavascriptBridge 4.1.5 https://github.com/marcuswestin/WebViewJavascriptBridge
 * 
 * SDWebImage 3.7.5 https://github.com/rs/SDWebImage
 *
 * FCUUID 1.1.5 https://github.com/fabiocaccamo/FCUUID
 *
 * DDVersion 1.0.0 https://github.com/Dids/DDVersion
 *
 * DDCarrier 1.0.0 https://github.com/Dids/DDCarrier
 *
 * UIAlertView+Blocks 0.9.0 https://github.com/ryanmaxwell/UIAlertView-Blocks
 *
 * NSString-CompareToVersion 0.3.0 https://github.com/stijnster/NSString-compareToVersion
 *
 * Google Sign-In SDK 2.4.0 https://developers.google.com/identity/sign-in/ios/sdk/
 *
 * Facebook SDK 4.10.0 https://developers.facebook.com/docs/ios/getting-started#sdk
 *
 */
